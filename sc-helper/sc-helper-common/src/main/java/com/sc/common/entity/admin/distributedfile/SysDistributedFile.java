package com.sc.common.entity.admin.distributedfile;

import com.sc.common.entity.BaseEntity;

/**
 * @author: wust
 * @date: 2020-03-08 15:25:30
 * @description:
 */
public class SysDistributedFile extends BaseEntity {
    private String bucketName;
    private String objectName;
    private String fileName;
    private Long size;
    private String fileType;
    private String source;
                        

    public void setBucketName (String bucketName) {this.bucketName = bucketName;}
    public String getBucketName(){ return bucketName;} 
    public void setObjectName (String objectName) {this.objectName = objectName;} 
    public String getObjectName(){ return objectName;} 
    public void setFileName (String fileName) {this.fileName = fileName;} 
    public String getFileName(){ return fileName;} 
    public void setSize (Long size) {this.size = size;} 
    public Long getSize(){ return size;} 
    public void setFileType (String fileType) {this.fileType = fileType;} 
    public String getFileType(){ return fileType;} 
    public void setSource (String source) {this.source = source;} 
    public String getSource(){ return source;} 
                        
}