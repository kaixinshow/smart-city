package com.sc.common.enums;

/**
 * 经常使用的数据字典，使用枚举来避免程序中的硬编码
 */
public enum DataDictionaryEnum {

    ORGANIZATION_TYPE_AGENT("A101101"),   // 组织架构类别：代理商
    ORGANIZATION_TYPE_PARENT_COMPANY ("A101104"),   // 组织架构类别：总公司
    ORGANIZATION_TYPE_BRANCH_COMPANY("A101107"),   // 组织架构类别：分公司
    ORGANIZATION_TYPE_PROJECT("A101109"),   // 组织架构类别：项目
    ORGANIZATION_TYPE_DEPARTMENT("A101111"),   // 组织架构类别：部门
    ORGANIZATION_TYPE_ROLE("A101113"),   // 组织架构类别：角色
    ORGANIZATION_TYPE_USER("A101115"),   // 组织架构类别：用户

    USER_TYPE_AGENT("A100403"), // 用户类型：代理商管理账号
    USER_TYPE_PARENT_COMPANY("A100406"), // 用户类型： 总公司管理账号
    USER_TYPE_BRANCH_COMPANY("A100409"), // 用户类型： 分公司管理账号
    USER_TYPE_PROJECT("A100410"), // 用户类型： 项目管理账号
    USER_TYPE_BUSINESS("A100411"); // 用户类型： 业务员账号



    private String stringValue;

    public String getStringValue() {
        return stringValue;
    }

    DataDictionaryEnum() {
    }

    DataDictionaryEnum(String stringValue) {
        this.stringValue = stringValue;
    }
}
