package com.sc.common.properties;



/**
 * @author ：wust
 * @date ：Created in 2019/9/19 16:10
 * @description：消费端配置类
 * @version:
 */
public class MqttInboundProperties {
    private String uri;

    private String username;

    private String password;

    private String clientId;

    private String[] topics;

    private int[] qos;


    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String[] getTopics() {
        return topics;
    }

    public void setTopics(String[] topics) {
        this.topics = topics;
    }

    public int[] getQos() {
        return qos;
    }

    public void setQos(int[] qos) {
        this.qos = qos;
    }
}
