package com.sc.common.enums;

public enum EhcacheKeyEnum {
    EHCACHE_KEY_SELECT_BY_PRIMARY_KEY("%s_%s_EHCACHE_KEY_SELECT_BY_PRIMARY_KEY"),
    EHCACHE_KEY_SELECT_ONE("%s_%s_EHCACHE_KEY_SELECT_ONE"),
    EHCACHE_KEY_SELECT_ONE_EXAMPLE("%s_%s_EHCACHE_KEY_SELECT_ONE_EXAMPLE");


    EhcacheKeyEnum(String stringValue){
        this.stringValue = stringValue;
    }

    private String stringValue;
    public String getStringValue() {
        return stringValue;
    }
}
