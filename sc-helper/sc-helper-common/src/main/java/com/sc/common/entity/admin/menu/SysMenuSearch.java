package com.sc.common.entity.admin.menu;

import com.sc.common.dto.PageDto;

/**
 * Created by wust on 2019/4/28.
 */
public class SysMenuSearch extends SysMenu {
    private static final long serialVersionUID = 6242560881745133655L;

    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}
