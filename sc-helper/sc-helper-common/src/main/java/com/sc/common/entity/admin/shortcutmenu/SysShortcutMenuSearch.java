package com.sc.common.entity.admin.shortcutmenu;

import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: 2020-02-16 11:25:19
 * @description:
 */
public class SysShortcutMenuSearch extends SysShortcutMenu {
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}