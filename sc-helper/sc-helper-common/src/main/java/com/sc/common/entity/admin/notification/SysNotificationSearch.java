/**
 * Created by wust on 2019-10-21 14:27:53
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.common.entity.admin.notification;

import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: Created in 2019-10-21 14:27:53
 * @description:
 *
 */
public class SysNotificationSearch extends SysNotification {
    private static final long serialVersionUID = -4288715460244087709L;

    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}
