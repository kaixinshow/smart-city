package com.sc.generatorcode.task;

import com.sc.generatorcode.entity.ColumnInfo;
import com.sc.generatorcode.task.base.AbstractTask;
import com.sc.generatorcode.utils.ConfigUtil;
import com.sc.generatorcode.utils.FileUtil;
import com.sc.generatorcode.utils.FreemarkerConfigUtils;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：wust
 * @date：2020-06-30
 */
public class ImportExcelXmlTask extends AbstractTask {
    public ImportExcelXmlTask(String className, List<ColumnInfo> infos) {
        super(className,infos);
    }

    @Override
    public void run() throws IOException, TemplateException {
        String name = super.getName()[1];
        String entityPackageName = ConfigUtil.getConfiguration().getBasePackage().getEntityPackageName() + "." + name;

        String xmlDir = FileUtil.getResourcePath().concat("/easyexcel/import/xml");

        StringBuffer fields = new StringBuffer();
        if(columnInfos != null && columnInfos.size() > 0){
            int i = 0;
            for (ColumnInfo columnInfo : columnInfos) {
                if("id".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("createrId".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("createrName".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("createTime".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("modifyId".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("modifyName".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("modifyTime".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("projectId".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("companyId".equals(columnInfo.getPropertyName())){
                    continue;
                }
                fields.append("<field index=\"" + (i++) + "\" property=\"" + columnInfo.getPropertyName() + "\"></field>\n");
            }
        }

        Map<String, String> data = new HashMap<>();
        String xmlName = ConfigUtil.getConfiguration().getServerName().concat("-").concat(name).replaceAll("-","_");
        data.put("xmlName", xmlName);
        data.put("importEntityClass", className.concat("Import"));
        data.put("fields", fields.toString());
        data.put("EntityPackageName", entityPackageName);

        String fileName = xmlName.concat(".xml");
        FileUtil.generateFile(FreemarkerConfigUtils.TYPE_IMPORT_EXCEL_XML, data, xmlDir,fileName);
    }
}
