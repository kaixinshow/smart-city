package com.sc.gateway;

import com.sc.common.service.ExportExcelServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;


@EnableEurekaClient
@ComponentScan(basePackages =
        {"com.sc"},
        excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
                classes = {
                        ExportExcelServiceImpl.class
                }
        )
)
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class GatewayServerApplication {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(GatewayServerApplication.class);
        app.run(GatewayServerApplication.class, args);
    }
}
