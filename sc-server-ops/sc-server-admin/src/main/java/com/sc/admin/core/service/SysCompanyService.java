package com.sc.admin.core.service;


import com.sc.common.entity.admin.company.SysCompany;
import com.sc.common.service.BaseService;


/**
 * Created by wust on 2019/6/3.
 */
public interface SysCompanyService extends BaseService<SysCompany> {
}
