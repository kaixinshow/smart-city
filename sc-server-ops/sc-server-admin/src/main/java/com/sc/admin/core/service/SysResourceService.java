package com.sc.admin.core.service;


import com.sc.common.entity.admin.resource.SysResource;
import com.sc.common.service.BaseService;

import java.util.List;

public interface SysResourceService extends BaseService<SysResource> {
    /**
     * 超级管理员：获取非白名单资源权限
     * @param type
     * @param lang
     * @return
     */
    List<SysResource> findAllResources4superAdmin(String type, String lang);


    /**
     * 超级管理员：获取白名单资源权限
     * @param type
     * @param lang
     * @return
     */
    List<SysResource> findAllAnonResources4superAdmin(String type,String lang);



    /**
     * 普通管理员：获取非白名单资源权限
     * @param permissionType
     * @param lang
     * @param accountId
     * @return
     */
    List<SysResource> findResources4admin(String permissionType, String lang, Long accountId);


    /**
     * 普通管理员：白名单资源权限
     * @param permissionType
     * @param lang
     * @param accountId
     * @return
     */
    List<SysResource> findAnonResources4admin(String permissionType,String lang,Long accountId);

    /**
     * 员工：非白名单资源权限
     * @param type
     * @param lang
     * @param userId
     * @return
     */
    List<SysResource> findResourcesByUserId(String type,String lang,Long userId);


    /**
     * 员工：白名单资源权限
     * @param type
     * @param lang
     * @param userId
     * @return
     */
    List<SysResource> findAnonResourcesByUserId(String type,String lang,Long userId);
}
