package com.sc.admin.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.project.SysProject;

public interface SysProjectMapper extends IBaseMapper<SysProject> {
}
