package com.sc.workorder.entity;

import com.sc.common.entity.BaseEntity;

/**
 * @author: wust
 * @date: 2020-04-01 10:27:05
 * @description:
 */
public class WoWorkOrder extends BaseEntity {
    private String workOrderNumber;
    private String workOrderName;
    private Long workOrderTypeId;
    private Long director;
    private String state;
    private Long applicant;
    private String priorityLevel;
    private String description;
    private Long projectId;
    private Long companyId;

    public String getWorkOrderNumber() {
        return workOrderNumber;
    }

    public void setWorkOrderNumber(String workOrderNumber) {
        this.workOrderNumber = workOrderNumber;
    }

    public String getWorkOrderName() {
        return workOrderName;
    }

    public void setWorkOrderName(String workOrderName) {
        this.workOrderName = workOrderName;
    }

    public Long getWorkOrderTypeId() {
        return workOrderTypeId;
    }

    public void setWorkOrderTypeId(Long workOrderTypeId) {
        this.workOrderTypeId = workOrderTypeId;
    }

    public Long getDirector() {
        return director;
    }

    public void setDirector(Long director) {
        this.director = director;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getApplicant() {
        return applicant;
    }

    public void setApplicant(Long applicant) {
        this.applicant = applicant;
    }

    public String getPriorityLevel() {
        return priorityLevel;
    }

    public void setPriorityLevel(String priorityLevel) {
        this.priorityLevel = priorityLevel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
}