package com.sc.workorder.entity;

import com.sc.common.entity.BaseEntity;

/**
 * @author: wust
 * @date: 2020-04-01 10:10:29
 * @description:
 */
public class WoWorkOrderType extends BaseEntity {
    private String name;
    private Long projectId;
    private Long companyId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
}