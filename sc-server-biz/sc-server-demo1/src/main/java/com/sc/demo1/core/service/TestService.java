package com.sc.demo1.core.service;

import com.sc.common.service.BaseService;
import com.sc.demo1.entity.test.Test1;

/**
 * @author: wust
 * @date: 2020-07-04 13:40:17
 * @description:
 */
public interface TestService extends BaseService<Test1>{
}
