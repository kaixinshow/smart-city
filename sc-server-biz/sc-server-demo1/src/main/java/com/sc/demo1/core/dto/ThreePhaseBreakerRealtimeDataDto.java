/**
 * Created by wust on 2019-10-16 14:57:00
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.demo1.core.dto;


import com.sc.common.util.MyStringUtils;

/**
 * @author: wust
 * @date: Created in 2019-10-16 14:57:00
 * @description: 三相断路器实时数据 数据传输对象
 *
 */
public class ThreePhaseBreakerRealtimeDataDto implements java.io.Serializable{
    private static final long serialVersionUID = 3262872203531915695L;


    private String gatewayMac;

    private String breakerSn;

    /**
     * 总状态
     */
    private String status;

    /**
     * 总电压
     */
    private String voltage;


    /**
     * 总电流
     */
    private String electricalCurrent;


    /**
     * 总电量
     */
    private String electricity;

    /**
     * 总漏电电流
     */
    private String leakageCurrent;

    /**
     * 总有功功率
     */
    private String power;

    /**
     * 总无功功率
     */
    private String reactivePower;

    /**
     * 功率因素
     */
    private String powerFactor;



    /**
     * A电压
     */
    private String voltageA;


    /**
     * A电流
     */
    private String electricalCurrentA;


    /**
     * A漏电电流
     */
    private String leakageCurrentA;

    /**
     * A有功功率
     */
    private String powerA;

    /**
     * A无功功率
     */
    private String reactivePowerA;

    /**
     * A功率因素
     */
    private String powerFactorA;

    /**
     * A温度
     */
    private String temperatureA;



    /**
     * B电压
     */
    private String voltageB;


    /**
     * B电流
     */
    private String electricalCurrentB;


    /**
     * B漏电电流
     */
    private String leakageCurrentB;

    /**
     * B有功功率
     */
    private String powerB;

    /**
     * B无功功率
     */
    private String reactivePowerB;

    /**
     * B功率因素
     */
    private String powerFactorB;

    /**
     * B温度
     */
    private String temperatureB;




    /**
     * C电压
     */
    private String voltageC;


    /**
     * C电流
     */
    private String electricalCurrentC;


    /**
     * C漏电电流
     */
    private String leakageCurrentC;

    /**
     * C有功功率
     */
    private String powerC;

    /**
     * C无功功率
     */
    private String reactivePowerC;

    /**
     * C功率因素
     */
    private String powerFactorC;

    /**
     * C温度
     */
    private String temperatureC;


    /**
     * N电流
     */
    private String electricalCurrentN;

    /**
     * N温度
     */
    private String temperatureN;

    /**
     * 接收数据时间
     */
    private String dateTimeByReceiving ;


    public String getGatewayMac() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(gatewayMac))){
            return "/";
        }
        return gatewayMac;
    }

    public void setGatewayMac(String gatewayMac) {
        this.gatewayMac = gatewayMac;
    }

    public String getBreakerSn() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(breakerSn))){
            return "/";
        }
        return breakerSn;
    }

    public void setBreakerSn(String breakerSn) {
        this.breakerSn = breakerSn;
    }

    public String getStatus() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(status))){
            return "/";
        }
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVoltage() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(voltage))){
            return "/";
        }
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public String getElectricalCurrent() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(electricalCurrent))){
            return "/";
        }
        return electricalCurrent;
    }

    public void setElectricalCurrent(String electricalCurrent) {
        this.electricalCurrent = electricalCurrent;
    }

    public String getElectricity() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(electricity))){
            return "/";
        }
        return electricity;
    }

    public void setElectricity(String electricity) {
        this.electricity = electricity;
    }

    public String getLeakageCurrent() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(leakageCurrent))){
            return "/";
        }
        return leakageCurrent;
    }

    public void setLeakageCurrent(String leakageCurrent) {
        this.leakageCurrent = leakageCurrent;
    }

    public String getPower() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(power))){
            return "/";
        }
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getReactivePower() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(reactivePower))){
            return "/";
        }
        return reactivePower;
    }

    public void setReactivePower(String reactivePower) {
        this.reactivePower = reactivePower;
    }

    public String getPowerFactor() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(powerFactor))){
            return "/";
        }
        return powerFactor;
    }

    public void setPowerFactor(String powerFactor) {
        this.powerFactor = powerFactor;
    }

    public String getVoltageA() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(voltageA))){
            return "/";
        }
        return voltageA;
    }

    public void setVoltageA(String voltageA) {
        this.voltageA = voltageA;
    }

    public String getElectricalCurrentA() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(electricalCurrentA))){
            return "/";
        }
        return electricalCurrentA;
    }

    public void setElectricalCurrentA(String electricalCurrentA) {
        this.electricalCurrentA = electricalCurrentA;
    }

    public String getLeakageCurrentA() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(leakageCurrentA))){
            return "/";
        }
        return leakageCurrentA;
    }

    public void setLeakageCurrentA(String leakageCurrentA) {
        this.leakageCurrentA = leakageCurrentA;
    }

    public String getPowerA() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(powerA))){
            return "/";
        }
        return powerA;
    }

    public void setPowerA(String powerA) {
        this.powerA = powerA;
    }

    public String getReactivePowerA() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(reactivePowerA))){
            return "/";
        }
        return reactivePowerA;
    }

    public void setReactivePowerA(String reactivePowerA) {
        this.reactivePowerA = reactivePowerA;
    }

    public String getPowerFactorA() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(powerFactorA))){
            return "/";
        }
        return powerFactorA;
    }

    public void setPowerFactorA(String powerFactorA) {
        this.powerFactorA = powerFactorA;
    }

    public String getTemperatureA() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(temperatureA))){
            return "/";
        }
        return temperatureA;
    }

    public void setTemperatureA(String temperatureA) {
        this.temperatureA = temperatureA;
    }

    public String getVoltageB() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(voltageB))){
            return "/";
        }
        return voltageB;
    }

    public void setVoltageB(String voltageB) {
        this.voltageB = voltageB;
    }

    public String getElectricalCurrentB() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(electricalCurrentB))){
            return "/";
        }
        return electricalCurrentB;
    }

    public void setElectricalCurrentB(String electricalCurrentB) {
        this.electricalCurrentB = electricalCurrentB;
    }

    public String getLeakageCurrentB() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(leakageCurrentB))){
            return "/";
        }
        return leakageCurrentB;
    }

    public void setLeakageCurrentB(String leakageCurrentB) {
        this.leakageCurrentB = leakageCurrentB;
    }

    public String getPowerB() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(powerB))){
            return "/";
        }
        return powerB;
    }

    public void setPowerB(String powerB) {
        this.powerB = powerB;
    }

    public String getReactivePowerB() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(reactivePowerB))){
            return "/";
        }
        return reactivePowerB;
    }

    public void setReactivePowerB(String reactivePowerB) {
        this.reactivePowerB = reactivePowerB;
    }

    public String getPowerFactorB() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(powerFactorB))){
            return "/";
        }
        return powerFactorB;
    }

    public void setPowerFactorB(String powerFactorB) {
        this.powerFactorB = powerFactorB;
    }

    public String getTemperatureB() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(temperatureB))){
            return "/";
        }
        return temperatureB;
    }

    public void setTemperatureB(String temperatureB) {
        this.temperatureB = temperatureB;
    }

    public String getVoltageC() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(voltageC))){
            return "/";
        }
        return voltageC;
    }

    public void setVoltageC(String voltageC) {
        this.voltageC = voltageC;
    }

    public String getElectricalCurrentC() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(electricalCurrentC))){
            return "/";
        }
        return electricalCurrentC;
    }

    public void setElectricalCurrentC(String electricalCurrentC) {
        this.electricalCurrentC = electricalCurrentC;
    }

    public String getLeakageCurrentC() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(leakageCurrentC))){
            return "/";
        }
        return leakageCurrentC;
    }

    public void setLeakageCurrentC(String leakageCurrentC) {
        this.leakageCurrentC = leakageCurrentC;
    }

    public String getPowerC() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(powerC))){
            return "/";
        }
        return powerC;
    }

    public void setPowerC(String powerC) {
        this.powerC = powerC;
    }

    public String getReactivePowerC() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(reactivePowerC))){
            return "/";
        }
        return reactivePowerC;
    }

    public void setReactivePowerC(String reactivePowerC) {
        this.reactivePowerC = reactivePowerC;
    }

    public String getPowerFactorC() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(powerFactorC))){
            return "/";
        }
        return powerFactorC;
    }

    public void setPowerFactorC(String powerFactorC) {
        this.powerFactorC = powerFactorC;
    }

    public String getTemperatureC() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(temperatureC))){
            return "/";
        }
        return temperatureC;
    }

    public void setTemperatureC(String temperatureC) {
        this.temperatureC = temperatureC;
    }

    public String getElectricalCurrentN() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(electricalCurrentN))){
            return "/";
        }
        return electricalCurrentN;
    }

    public void setElectricalCurrentN(String electricalCurrentN) {
        this.electricalCurrentN = electricalCurrentN;
    }

    public String getTemperatureN() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(temperatureN))){
            return "/";
        }
        return temperatureN;
    }

    public void setTemperatureN(String temperatureN) {
        this.temperatureN = temperatureN;
    }

    public String getDateTimeByReceiving() {
        if(MyStringUtils.isBlank(MyStringUtils.null2String(dateTimeByReceiving))){
            return "/";
        }
        return dateTimeByReceiving;
    }

    public void setDateTimeByReceiving(String dateTimeByReceiving) {
        this.dateTimeByReceiving = dateTimeByReceiving;
    }
}
