package com.sc.admin.api;


import com.sc.admin.api.fallback.ProjectServiceFallback;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.project.SysProject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "admin-server",fallback = ProjectServiceFallback.class)
public interface ProjectService {
    String API_PREFIX = "/api/feign/v1/ProjectFeignApi";
    String SELECT = API_PREFIX + "/select";

    @RequestMapping(value =SELECT,method= RequestMethod.POST, consumes = "application/json")
    WebResponseDto select(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody SysProject search);
}
